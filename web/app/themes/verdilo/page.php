<?php get_header(); ?>

<div id="main" class="main full" role="main">
    <div class="inside">

        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
        ?>

            <header class="entry-header">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        <?php
        // End the loop.
        endwhile;
        ?>

    </div>
</div><!-- .site-main -->

<?php get_footer(); ?>
