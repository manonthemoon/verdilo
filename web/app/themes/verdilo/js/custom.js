jQuery(document).ready(function() {

    jQuery(".to-up a").click(function() {
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
});

jQuery(window).load(function() {
    jQuery('#featured').orbit({
        animation: 'fade',
        timer: false
    });
});