<?php


// enqueue scripts
function verdilo_scripts() {
    wp_enqueue_style( 'orbit', get_template_directory_uri() . '/css/orbit-1.2.3.css', array(), date('Ymd') );

    wp_enqueue_script( 'verdilo-to-top', get_template_directory_uri() . '/js/custom.js', array('jquery'), date('Ymd'), true );
    wp_enqueue_script( 'orbit', get_template_directory_uri() . '/js/jquery.orbit-1.2.3.js', array('jquery'), date('Ymd'), true );
}
add_action( 'wp_enqueue_scripts', 'verdilo_scripts' );


// add theme support for featured images
add_theme_support( 'post-thumbnails' );
add_image_size( 'full-width', 1120, 480, true );
add_image_size( 'one-third', 336, 250, true );
update_option( 'image_default_link_type', 'none' );

// add the menus
register_nav_menus( array(
    'header_left'     => __( 'Menu left', 'twentyfifteen' ),
    'header_right'    => __( 'Menu right', 'twentyfifteen' ),
    'homepage_slider' => __( 'Homepage slider menu', 'twentyfifteen' ),
    'social'          => __( 'Social links menu', 'twentyfifteen' ),
    'footer'          => __( 'Footer menu', 'twentyfifteen' ),
));


// add the sidebars (for the footer)
function verdilo_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer left', 'twentyfifteen' ),
        'id'            => 'footer-bar-left',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer center', 'twentyfifteen' ),
        'id'            => 'footer-bar-center',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'verdilo_widgets_init' );

// remove height and with from images
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}

function my_gallery_default_type_set_link( $settings ) {
    $settings['galleryDefaults']['link'] = 'file';
    return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');
