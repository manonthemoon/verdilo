<?php
/**
 * Template Name: Contact page
 */
?>

<?php get_header(); ?>

<div id="main" class="main full" role="main">
    <div class="inside">

        <div class="entry-content">

            <div id="slider">
                <div class="cat">
                    <?php foreach (wp_get_nav_menu_items(8) as $menu_item): ?>
                    <div class="cat-inner">
                        <a href="<?php echo $menu_item->url; ?>" title="<?php echo $menu_item->title; ?>"><?php echo $menu_item->title; ?></a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div id="featured">
                <?php
                $args = array( 'numberposts' => 3, 'orderby' => 'date', 'order' => 'DESC', 'featured' => 'yes' );
                $postslist = get_posts( $args );
                foreach ($postslist as $number => $post) :  setup_postdata($post); ?>
                    <div class="post-slider-item slider-item-<?php echo $number; ?>">
                        <div class="post-thumb">
                            <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo get_the_title(); ?>">
                                <?php the_post_thumbnail( 'full-width' ); ?>
                            </a>
                            <?php the_title( '<h4 class="entry-title font-red"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
                </div>
            </div>

            <div class="two">
                <h3 class="font-red">Nieuwste projecten</h3>
            </div>
            <div class="two last">
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button">Bekijk alle</a>
            </div>
        </div>

        <?php
        $args = array( 'numberposts' => 3 );
        $postslist = get_posts( $args );
        foreach ($postslist as $post) :  setup_postdata($post); ?>
            <div class="post-item">
                <div class="post-entry">
                    <div class="cat">
                        <?php $categories = get_the_category(); // $category = array_shift($categories); ?>
                        <?php foreach ($categories as $category): ?>
                        <div class="cat-inner">
                            <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->name; ?>">
                                <?php echo $category->cat_name; ?>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="thumbnail">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo get_the_title(); ?>">
                            <?php the_post_thumbnail('one-third'); ?>
                        </a>
                    </div>
                    <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
                    <div class="short-descr">
                        <p class="open-sans big"><?php the_field('street'); ?></p>
                        <?php the_field('short_description'); ?>
                    </div>
                </div>
                <p class="read-more open-sans big">
                    <a href="<?php echo esc_url( get_permalink() ); ?>">Lees meer</a>
                </p>
            </div>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>

    </div>
</div><!-- .site-main -->

<?php get_footer(); ?>
