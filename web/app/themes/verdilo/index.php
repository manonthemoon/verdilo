<?php get_header(); ?>
    <div id="main" class="main full" role="main">
        <div class="inside overview">

            <div class="cat-overview">
                <?php echo wp_list_categories(array(
                    'title_li' => ''
                )); ?>
            </div>

            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                ?>
                    <?php get_template_part( 'content-post' ); ?>
            <?php
                // End the loop.
            endwhile;
            ?>

        </div>
    </div><!-- .site-main -->

<?php get_footer(); ?>
