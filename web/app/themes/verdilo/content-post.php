<div class="post-item">
    <div class="post-entry">
        <div class="cat">
            <?php $categories = get_the_category(); //$category = array_shift($categories); ?>
            <?php foreach ($categories as $category): ?>
            <div class="cat-inner">
                <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->name; ?>">
                    <?php echo $category->cat_name; ?>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="thumbnail">
            <a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo get_the_title(); ?>">
                <?php the_post_thumbnail('one-third'); ?>
            </a>
        </div>
        <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
        <div class="short-descr">
            <p class="open-sans big"><?php the_field('street'); ?></p>
            <?php the_field('short_description'); ?>
        </div>
    </div>
    <p class="read-more open-sans big">
        <a href="<?php echo esc_url( get_permalink() ); ?>">Lees meer</a>
    </p>
</div>