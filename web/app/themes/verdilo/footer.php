<footer id="footer" class="footer full">
    <div class="inside">
        <div class="three first">
            <?php dynamic_sidebar( 'footer-bar-left' ); ?>
        </div>
        <div class="three">
            <?php dynamic_sidebar( 'footer-bar-center' ); ?>
        </div>
        <div class="three last">
            <div class="to-up">
                <a href="#">Terug<br />naar<br />boven</a>
            </div>
        </div>
    </div>
    <div class="inside credits">
        <p>
            &copy; Verdilo |
            <a href="/">Algemene voorwaarden</a> |
            Site by <a href="http://wizarts.be" target="_blank">Wizarts</a>
        </p>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>