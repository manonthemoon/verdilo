<?php
/**
 * Template Name: Contact page
 */
?>

<?php get_header(); ?>

<div id="main" class="main full" role="main">
    <div class="inside">

        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            ?>

            <header class="entry-header">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        <?php
            // End the loop.
        endwhile;
        ?>

    </div>
</div><!-- .site-main -->

<div class="full red">
    <div class="inside">
        <div class="two">
            <h2>Kantoor</h2>
            <p class="small">
                Kom gerust eens langs op kantoor tijdens onze openingsuren
            </p>
            <p>
                <span class="contact-date">Maandag</span>
                <span class="contact-hour">13u - 18u</span><br />
                <span class="contact-date">Dinsdag</span>
                <span class="contact-hour">13u - 18u</span><br />
                <span class="contact-date">Woensdag</span>
                <span class="contact-hour">13u - 18u</span><br />
                <span class="contact-date">Donderdag</span>
                <span class="contact-hour">13u - 18u</span><br />
                <span class="contact-date">Vrijdag</span>
                <span class="contact-hour">13u - 18u</span><br />
            </p>
        </div>
        <div class="two last">
            <p class="contact-text">
                Verdilo<br />
                Zavel 3<br />
                9080 Zeveneken
            </p>
            <p class="contact-text">
                T - 09 355 99 60<br />
                F - 09 356 96 33<br />
                <a href="mailto:info@verdilo.be" target="_blank">info@verdilo.be</a>
            </p>
        </div>
    </div>
</div>
<div class="full">
    <div class="inside">
        <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
    </div>
</div>
<div class="full">
    <?php echo do_shortcode('[pw_map address="Zavel 3, 9080 Zeveneken" height="400px" width="100%" enablescrollwheel="false" marker="' . get_template_directory_uri() . '/images/trans-2.png' . '"]'); ?>
</div>
<?php get_footer(); ?>
