<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<div id="main" class="main full" role="main">
    <div class="inside">
        <div class="entry-content">

            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            
            <div class="entry-cats">
			<?php $categories = get_the_category(); //$category = array_shift($categories); ?>
            <?php foreach ($categories as $category): ?>
                <a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->name; ?>">
                    <?php echo $category->cat_name; ?>
                </a>
            <?php endforeach; ?>
            </div>
            
			<div class="clear"></div>
            <div class="post-thumb">
                <?php the_post_thumbnail( 'full-width' ); ?>
            </div>

            <div class="post-content">
                <div class="inner">
                    <?php the_content(); ?>
                    <p class="great">
                        Ref. <?php the_field('reference'); ?>
                    </p>
                    <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button">
                        < Terug naar overzicht
                    </a>
                </div>
                <div class="post-contact">
                    <div class="price"><?php the_field('price'); ?> €</div>
                    <div class="location">
                        <?php the_field('street'); ?><br />
                        <?php the_field('zipcode'); ?>
                    </div>
                    <div class="last">
                        <?php the_field('short_description'); ?>
                    </div>
                    <a href="/over-ons-en-contact" class="button white">
                        Contacteer ons
                    </a>
                </div>
            </div>
        </div>
    </div>
 </div>
<?php endwhile; ?>

<div class="full">
    <?php $coordinates = get_post_meta(get_queried_object_id(), 'address'); ?>
    <?php $coordinates = 'lat=' . $coordinates[0]['lat'] . ' lng=' . $coordinates[0]['lng']; ?>
    <?php echo do_shortcode('[pw_map ' . $coordinates . ' address="' . get_field('street') . ', ' . get_field('zipcode') . ' Belgium" height="480px" width="100%" enablescrollwheel="false" marker="' . get_template_directory_uri() . '/images/trans-2.png' . '"]'); ?>
</div>
<?php get_footer(); ?>