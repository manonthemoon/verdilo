<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title( '|', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo " | $site_description";

        // Add a page number if necessary:
        if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
            echo esc_html( ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) ) );

        ?></title>
        
        <script src="//use.typekit.net/hmf1rer.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>

    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header full">
        <div class="inside">
            <div class="menu left-menu">
                <?php
                // Primary navigation menu.
                wp_nav_menu( array(
                    'menu_class'     => 'nav-menu',
                    'theme_location' => 'header_left',
                ) );
                ?>
            </div>
            <div class="site-branding">
                <?php
                if ( is_front_page() && is_home() ) : ?>
                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?>, <?php bloginfo( 'description' ); ?></a></h1>
                <?php else : ?>
                    <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?>, <?php bloginfo( 'description' ); ?></a></p>
                <?php endif; ?>
            </div><!-- .site-branding -->
            <div class="menu left-menu">
                <?php
                // Primary navigation menu.
                wp_nav_menu( array(
                    'menu_class'     => 'nav-menu',
                    'theme_location' => 'header_right',
                ) );
                ?>
            </div>
        </div>
    </header><!-- .site-header -->

    <div id="content" class="site-content">
